/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sania.filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author ASUS
 */
public class WriteFriend {
    public static void main(String[] args){
        FileOutputStream fos = null;
        try {
            Friend friend1 = new Friend("Sania",20,"0820877330");
            Friend friend2 = new Friend("Mu",21,"0625595778");
            Friend friend3 = new Friend("Dew",21,"0972406165");
            File file = new File("frind.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream OOS = new ObjectOutputStream(fos);
            OOS.writeObject(friend1);
            OOS.writeObject(friend2);
            OOS.writeObject(friend3);
            OOS.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
